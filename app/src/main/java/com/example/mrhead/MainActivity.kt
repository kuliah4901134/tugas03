package com.example.mrhead

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.mrhead.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.rambut.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked){
                binding.imgRambut.visibility=View.VISIBLE
            }else{
                binding.imgRambut.visibility = View.GONE
            }
        }
        binding.alis.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked){
                binding.imgAlis.visibility=View.VISIBLE
            }else{
                binding.imgAlis.visibility = View.GONE
            }
        }
        binding.kumis.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked){
                binding.imgKumis.visibility=View.VISIBLE
            }else{
                binding.imgKumis.visibility = View.GONE
            }
        }
        binding.janggut.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked){
                binding.imgJanggut.visibility=View.VISIBLE
            }else{
                binding.imgJanggut.visibility = View.GONE
            }
        }

    }
}